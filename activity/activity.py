name = "Jherson"
age = 23 
occupation = "Associate Engineer Technical Support"
movie = "inception"
rating = 95.5

print(f"My name is {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}%.")

num1, num2, num3 = 15, 7, 98

print(num1 * num2)

if num1 < num3:
    print("num1 is less than num3")
elif num1 >= num3:
    print("num1 is greater than or equal to num3")

print(num3 + num2)
