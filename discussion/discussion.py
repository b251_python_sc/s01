print ("hello world!")

# [Section] Declaring Variables
age = 29 
middle_initial = "D"

# name1 = "jherson"
# name2 = "jhersy"
# name3 = "denise"

# python allows assgning of values to multiple variables within a single line

name1, name2, name3, name4 = "john", "paul", "george", "ringo"

# [Section] Data Types 
# 1. Strings - alphanumeric characters and symbols  
full_name = "john doe"
secret_code = "pa$$word"

# 2. Numbers - for integers, decimals, and complex numbers
number_of_days = 366 # this is an integer
pi_approximation = 3.1416 # this is a decimal
complex_num = 1 + 5j #this is a complex number, represents an imaginary component

# 3. Boolean - true or false values
isLearning = True
isDifficult = False 

# [Section] Using variables
print ("my name is " + full_name)
print ("my age is " + str(age)) # str = convert into a string // typecasting parses data to convert it into a different data type

print(int(3.5)) # print to a regular integer // 3.5 > 3
print(float(5)) # converts integer to float value // 5 > 5.

# f strings // template literals
# Another way to concatenate  and it ignores the strict typiing needed with  regular concatenation  using the "+" operator
print(f"hi my name is {full_name} and my age is {age}") 

# [Section] Operations
# arithmetic operations - for mathematical oprerations
print (1 + 10)
print (15 - 8)
print (18 * 9)
print (21 / 7)
print (18 % 4 ) # modulo - gets the remainder of division
print (2 ** 6)

# Assignment Operators - for assigning values to variables
num1 = 3
num1 += 4 
print(num1)

# Comparison Operators - if else statements 
print(1 == 1)

# Logical Operators
print (True and False)
print (not False)
print (False or True)
